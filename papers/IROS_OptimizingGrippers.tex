%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%2345678901234567890123456789012345678901234567890123456789012345678901234567890
%        1         2         3         4         5         6         7         8

\documentclass[letterpaper, 10 pt, conference]{ieeeconf}  % Comment this line out if you need a4paper

%\documentclass[a4paper, 10pt, conference]{ieeeconf}      % Use this line for a4 paper

\IEEEoverridecommandlockouts                              % This command is only needed if 
                                                          % you want to use the \thanks command

\overrideIEEEmargins                                      % Needed to meet printer requirements.

% See the \addtolength command later in the file to balance the column lengths
% on the last page of the document

% The following packages can be found on http:\\www.ctan.org
\usepackage{graphicx} % for pdf, bitmapped graphics files
\usepackage{epsfig} % for postscript graphics files
\usepackage{epstopdf}
\usepackage{mathptmx} % assumes new font selection scheme installed
\usepackage{times} % assumes new font selection scheme installed
\usepackage{amsmath} % assumes amsmath package installed
\usepackage{bm}
\usepackage{amssymb}  % assumes amsmath package installed
%\usepackage{subfig}
\usepackage{algorithm}
\usepackage{color}
\usepackage{algpseudocode}
\usepackage{tikz}
\usepackage[english]{babel}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{multirow}
\usepackage{subfigure}
\usepackage{todonotes}
\usepackage{csvsimple}
\usepackage{placeins}
\usepackage{cite}
\graphicspath{{./}}

% Define your comments here!
\newcommand{\comment}[1]{{\color{red} Comment: #1}}
\newcommand{\commentAW}[1]{{\color{green} AW: #1}}
\newcommand{\commentNK}[1]{{\color{red} NK: #1}}
\newcommand{\commentTNT}[1]{{\color{orange} TNT: #1}}

\newcommand{\Figure}[0]{Fig.~}
\newcommand{\Figures}[0]{Figs.~}
% when used at the beginning of a sentence
\newcommand{\FigureBegin}[0]{Fig.~}
\newcommand{\FiguresBegin}[0]{Figs.~}
\newcommand{\Table}[0]{Tab.~}

\newcommand{\Section}[0]{Sect.~}
\newcommand{\Sections}[0]{Sects.~}
\newcommand{\SectionBegin}[0]{Sect.~}
\newcommand{\SectionsBegin}[0]{Sects.~}

\newcommand{\Equation}[0]{Eq.~}
\newcommand{\Algorithm}[0]{Alg.~}

\newcommand{\remove}[1]{}

\title{\LARGE \bf
Optimizing Grippers for Compensating Pose Uncertainties by Dynamic Simulation
}


\author{Adam Wolniakowski$^{1}$, Alja\v{z} Kramberger$^{2}$, Andrej Gams$^{2}$,  Dimitrios Chrysostomou$^{3}$, \\ Frederik Hagelskj{\ae}r$^{4}$, Thomas Nicky Thulesen$^{4}$, Lilita Kiforenko$^{4}$, Anders Glent Buch$^{4}$, \\ Leon Bodenhagen$^{4}$, Henrik Gordon Petersen$^{4}$, Ole Madsen$^{3}$, Ale\v{s} Ude$^{2}$, Norbert Kr\"{u}ger$^{4}$ % <-this % stops a space
\thanks{$^{1}$Adam Wolniakowski is with Faculty of Mechanical Engineering,
        Białystok University of Technology, 15-351 Białystok, Poland
        {\tt\small adam.wolniakowski@gmail.com}}%
\thanks{$^{2}$Andrej Gams, Alja\v{z} Kramberger, and Ale\v{s} Ude are with the Jo\v{z}ef Stefan Institute, 1000 Ljubljana, Slovenia
        {\tt\small \{aljaz.kramberger, andrej.gams, ales.ude\}@ijs.si}}%
\thanks{$^{3}$Dimitrios Chrysostomou, and Ole Madsen are with the Aalborg University, DK-9220 Aalborg East, Denmark
        {\tt\small \{dimi, om\}@m-tech.aau.dk}}%
\thanks{$^{4}$Frederik Hagelskj{\ae}r, Thomas Nicky Thulesen, Lilita Kiforenko, Anders Glent Buch, Leon Bodenhagen, Henrik Gordon Petersen, and Norbert Kr\"{u}ger are with the The Maersk Mc-Kinney Moller Institute, University of Southern Denmark, DK-5230 Odense M, Denmark
        {\tt\small \{frhag, tnt, lilita, anbu, lebo, hgp, norbert\}@mmmi.sdu.dk}}%
}


\begin{document}



\maketitle
\thispagestyle{empty}
\pagestyle{empty}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}
The gripper design process is one of the interesting challenges in the context of grasping within industry. Typically, simple parallel-finger grippers, which are easy to install and maintain, are used in platforms for robotic grasping. The context switches in these platforms require frequent exchange of gripper fingers to accommodate grasping of new products, while subjected to numerous constraints, such as workcell uncertainties due to the vision systems used. The design of these fingers consumes the man-hours of experienced engineers, and involves a lot of trial-and-error testing.

In our previous work, we have presented a method to automatically compute the optimal finger shapes for defined task contexts in simulation. In this paper, we show the performance of our method in an industrial grasping scenario. We first analyze the uncertainties of the used vision system, which are the major source of grasping error. Then, we perform the experiments, both in simulation and in real setting. The experiments confirmed the validity of our approach. The computed finger design was employed in a real industrial assembly scenario.
\end{abstract}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{INTRODUCTION}

Grasping based on pose estimation using vision has to deal with vision induced uncertainties. These uncertainties are related to the quality of the generated point cloud, the sensor choice as such, the geometrical relation of the sensor to the object and the number of sensors used. Often a rough estimate of the uncertainty imposed by the vision system can be found in the form of positional and angular uncertainties by simply analyzing the variance of different pose estimate samples under naturally occurring illumination conditions.
 
Assuming that an estimate of the vision induced uncertainty exists, the gripper design can be chosen to compensate for the expected pose uncertainties by introducing a cutout in the robot finger (see \Figure\ref{fig:overview}). Different types of such cutouts  exist, such as cutouts based on molding ("inverse" of the object) \cite{SchunkTool}, convex hulls \cite{ellekilde_2006}, or simple geometric primitives (prismatic, round). In former work \cite{Woln15}, we have shown how to learn optimal finger parameters for two-finger grippers in simulation, where one of the objectives was to enhance the gripper's alignment capability. 

In this work, we describe an application in which first rough estimates of visual uncertainties are made for two pose estimation tasks (see \Figure \ref{fig:basin_of_convergence}). We then compute grippers that compensate for these uncertainties in simulation (see \Figure \ref{fig:alignment_landscape}). To verify the alignment capabilities of the computed grippers, we have applied the gripper design in a real world experiment on a test platform (see \Figure \ref{fig:magnet_setup} and \ref{fig:rotorcap_setup}). We then integrate the computed cutouts in a  gripper that is supposed to handle a number of grasping tasks (see \Figure \ref{fig:acat_finger}) and applied it in an industrial application context. 

By that, we have closed the circle between vision induced uncertainties, gripper design and required alignment properties. Hence our methodology allows for simplifying the set-up of robot assembly systems, by replacing the design problem of grippers by an automatic procedure based on uncertainty estimation from vision, gripper optimization and 3D printing.


 
% --- fig: overview ---
  \begin{figure*}[!tb]
    \centering
    \begin{tabular}{cc}
      \includegraphics[width=0.85\linewidth]{figures/big_picture}
    \end{tabular}
    \caption{Overview of the problem. The pose estimation yields inexact results suffering from the uncertainties in the vision systems, which makes the grasping challenging (left). A proper finger design compensates for these uncertainties, resulting in successful grasping (right). }
    \label{fig:overview}
  \end{figure*}
% --- /fig: overview ---

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{STATE OF THE ART}
In this Section we give an overview of the research previously done in the areas of grasping, gripper design, and the pose estimation using vision systems.

\subsection{Gripper Design in Industry}
It is increasingly  common for robotic grasping systems to replace manual labour in the industry. These systems have to meet requirements on efficiency and reliability, in order to be economically viable. The gripper design is a critical part of implementing such a robotized solution. Nowadays, it is most common to use parallel-finger grippers in industrial settings. The fingers are developed by experienced engineers, with design choices based on human expertise, and a costly and time-consuming trial-and-error process.

There are various guidelines available to assist in the gripper design problem, i.e. \cite{causey_gripper_1998,causey_2003,Krenich_2004,causey_elements_1999}. They emphasise the reliability of the gripper, increase in the system throughput, and cost optimization. These objectives are often conflicting, but a good compromise can be achieved by minimizing gripper weight, reducing its footprint, including cutouts in the finger to retain the objects securely, and other similar means.

The gripper jaw design for the purpose of secure grasping and object alignment has also been studied extensively in \cite{Zhang_2001,ZhangCG01,zhang_2006}. In \cite{zhang_2006} a parameterization of a modular gripper surface is introduced, and an optimization algorithm to achieve specific object alignment is presented. In \cite{ellekilde_2006}, the convex-hull based finger shapes are investigated in terms of alignment.

Recently, Schunk GmbH has made an online tool available \cite{SchunkTool}, which can generate finger shapes based on the object molding for simple grasping scenarios.

\subsection{Gripper Learning in Simulation}
Simulation as a tool in the context of grasping and gripper design is mostly encountered in problems of optimal grasp planning and structural design \cite{Nikandrova2015, ciocarlie_2010, Kolluru00}. In our previous research, we have used dynamic simulation to obtain feedback in the finger geometry design phase \cite{Woln14, Woln15}. In this work, using an approach similar to \cite{ellekilde_2006}, we aim at confirming the relevance of such feedback by presenting and comparing results obtained from the experiments performed in both simulated and the real-world settings.

\subsection{Pose Estimation}
The Microsoft Kinect and similar (e.g. Carmine) sensors became popular in computer vision community since the launch in November 2010, because they allow us to extract the RGB and depth information fast and at a low cost. Kinect cameras are not designed with the focus of precision, they are designed for gaming or other human-computer interaction. Therefore it comes with some limitations, such as sensor noise which introduces depth uncertainties. Several studies have been conducted about the noise and reconstruction uncertainties of Kinect point cloud \cite{Olesen2012,Belhedi2015}. 

A large body of work has been produced over the last three decades with the aim of recognizing and performing pose estimation on 3D objects in point clouds. A notable initial work used the local Spin Image descriptor \cite{johnson1999using} for finding corresponding regions between the object model and a scene. The same principle of using local surface descriptors has since been heavily revisited \cite{frome2004recognizing,mian2006three,rusu2009fast,guo2013rotational,Kiforenko2014,salti2014}. A recent survey \cite{guo20143d} provides a comprehensive overview of available 3D descriptors and recognition systems. Furthermore, extensive evaluations of such methods have been carried out \cite{guo2016comprehensive}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{METHODS}
In our approach, we first analyze the uncertainties of the pose estimation from the vision system used (Carmine) to define the uncertainties relevant for the grasping task (\Section\ref{sec:pose_estimation}). We then use the task context as the input to our finger design optimization method to obtain the optimal finger geometry for the two objects considered: the magnet and the rotorcap (\Section\ref{sec:design_learning}).

\subsection{Pose Estimation}\label{sec:pose_estimation}
The pose uncertainty results for the magnet and rotorcap objects were computed with methods based on edges, the shape and a method in which features are combined, as described in \cite{Kiforenko2015}. 

The method uses a combination of different descriptors. For each object, we have a 3D model available. First we extract both edge and surface points from the scene and object point clouds. Then, for each of the edge/surface points, we compute different descriptors, both using the edge and the surface information. For this experiment we used two local histogram based descriptors - SHOT \cite{salti2014} and Cat3DEdge \cite{Kiforenko2014}. SHOT is a descriptor that performs well for the objects with rich surface data and Cat3DEdge performs well for planar objects by relying on edge data. For both descriptors for each object point we find the closest match in a scene - a potential correspondent point. We concatenate two correspondence vectors into one and use it as input for the RANSAC algorithm that provides as a 6D pose of the best match.

The result of pose uncertainties are presented in \Table\ref{tab:vision_uncertainties}. The result shows the variance of the pose, by estimation of the object pose in 100 consequential scenes by using only the edge points (Cat3DEdge) descriptor, surface point (SHOT) descriptor and both combined. The variance of detection using only the edge descriptor is lower compared to the surface descriptor for the magnet object. For the rotorcap object it is opposite. By combining both descriptors, we get a result in-between. It allows us to have a more universal method that on average gives less detection variance. The result also shows a high variance in rotation for the rotorcap object. The reason is that this object is symmetrical and therefore has one degree of freedom along the z-axis.   

%\commentNK{I do not understand why combined does not give optimal or at least averag eperformanve. There we need to have made something wrong. If we cannot fix it, we should not mention teh combined results.}


% --- tab: vision_uncertainties ---
\begin{table}
  \caption{Pose uncertainty result for magnet and rotorcap object}
  \begin{tabular}{|c|c|c|c|}
  \hline
    \multirow{2}{*}{Object}& \multirow{2}{*}{Features} & \multicolumn{2}{|c|}{Error measure}\\ \cline{3-4}
  &  &  Translation [mm]& Rotation [$^\circ$]\\ \cline{2-2}
  \hline \hline
  \multirow{3}{*}{Magnet} &edge & 2.426 & 0.214720\\\cline{2-4}
   & surface points & 3.614 & 0.463715\\ \cline{2-4}
   & combined &  2.430 & 0.214765\\ 
  \hline \hline
  \multirow{3}{*}{Rotorcap} &edge & 9.4 & 4.9976\\\cline{2-4}
   & surface points & 4.9 & 2.8400\\ \cline{2-4}
   & combined & 7.75 & 11.8831\\ 
  \hline
  \end{tabular}
  
  \label{tab:vision_uncertainties}
\end{table}
% --- /tab: vision_uncertainties ---

\subsection{Gripper Design Learning}\label{sec:design_learning}
The optimal gripper design for the objects in the considered industrial scenario (\textit{magnet} and \textit{rotorcap}) has been learned using the methods presented in \cite{Woln15}. We describe the main points of the method below for the sake of completeness.

First, we define an objective function that quantitatively describes the quality of the tested gripper design. Such a function is a weighted product of individual Quality Indices, each representing a different facet of the grasping process \cite{Woln14}:
\begin{itemize}
\item \textbf{Success Index} $S$, which represents the probability of grasp success.
\item \textbf{Robustness Index} $R$, representing the stability of grasp success in the presence of noise.
\item \textbf{Alignment Index} $A$, describing the ability of the gripper to position grasped objects in a determined way.
\item \textbf{Coverage Index} $C$, illustrating the versatility of the gripper in executing successful grasps from multiple directions.
\item \textbf{Wrench Index} $W$, which quantifies the quality of executed grasps in terms of forces necessary to dislodge the object.
\item \textbf{Stress Index} $\zeta$, which describes the structural robustness of the gripper design.
\item \textbf{Volume Index} $V$, representing the cost of producing the set of fingers (in terms of volume of the material used).
\end{itemize}

Since in the considered industrial case, we are mostly concerned  with the alignment properties of the gripper, we have selected the following weights for the objective function:
\begin{align*}\label{eq:alignment-weights}
w_S &= 0 \hspace{2.5mm} w_R = 0 \hspace{2.5mm} w_A = 1 \hspace{2.5mm} w_C = 0 \\
w_W &= 1 \hspace{2.5mm} w_\zeta = 0.01 \hspace{2.5mm} w_V = 0.01 \nonumber
\end{align*}

We also define several parametrizations of the gripper finger shape (primitive geometry cutouts: prismatic, round, trapezoid). The gripper design evaluation is done in a simulated environment, in which the grasping task from the real-world setting is reproduced with varied perturbations. The quantitative design evaluation is subsequently fed into an optimization method, that searches through the parameter space for the optimal configuration. In this work, we have used the \textit{coordinate descent} approach \cite{Wright2015} to select the best parameter values.  \Figure\ref{fig:alignment_landscape} shows the quality objective values for the \textit{cut width} parameter linear search in the \textit{rotorcap} grasping scenario.
\begin{figure}[!t]
  \centering
  \includegraphics[width=0.85\linewidth]{figures/alignment_landscape}
  \caption{The final linear search step in the coordinate descent optimization of the \textit{cut width} parameter of the \textit{rotorcap} cutout. The orange line shows the value of the \textit{Alignment Index} ($w_A$), the blue -- \textit{Wrench Index} ($w_W$), the cyan -- \textit{Volume Index} ($W_V$), the pink -- \textit{Stress Index} ($w_\zeta$), and the solid black line -- the total quality objective.}
  \label{fig:alignment_landscape}
\end{figure}

Such procedure was performed for all objects of the industrial assembly scenario within the FP-7 project ACAT (magnet, rotorcap, rotorshaft, and ring). For each of them, we have optimized the individual cutout shape. These cutouts have then been merged into one finger design (see \Figure\ref{fig:acat_finger}), suitable for all of the objects. In this work, we focus on the \textit{magnet} and \textit{rotorcap} objects, and their corresponding cutouts. The cutout geometry and parameters for the considered objects is presented in \Figure\ref{fig:learning_results}.

% --- fig: learning_results ---
\begin{figure}[!tbh]
  \centering
  \begin{tabular}{|c|c|}
    \hline Magnet & Rotorcap \\ \hline
    \includegraphics[width=0.33\linewidth,height=2.5cm]{figures/magnet_cutout2} &
    \includegraphics[width=0.33\linewidth,height=2.5cm]{figures/rotorcap_cutout} \\ \hline
    Trapezoid cutout & Round cutout \\
    depth: 1 mm & depth: 10 mm \\
    width: 2.4 mm & diameter: 32.5 mm \\
    TCP off.: 3 mm & TCP off.: 45 mm \\
    angle (prox.): 90$^\circ$ & \\
    angle (dist.): 110$^\circ$ & \\ \hline
  \end{tabular}
  \caption{Optimal cutout geometries found for the magnet and for the rotorcap.}
  \label{fig:learning_results}
\end{figure}
% --- /fig: learning_results ---

% --- fig: acat_finger ---
\begin{figure}[!b]
  \centering
  \includegraphics[width=0.85\linewidth]{figures/acat_finger_labels}
  \caption{The combined finger design with four separately learned cutouts for the ACAT objects (magnet, ring, rotorcap, rotorshaft).}
  \label{fig:acat_finger}
\end{figure}
% --- /fig: acat_finger ---

% --- fig: basin_of_convergence ---
  \begin{figure*}[!tb]
    \centering
    \includegraphics[width=0.85\linewidth]{figures/basins}
    \caption{Basins of convergence for the magnet and for the rotorcap object. Light red shows the worst case of vision pose estimation uncertainty (see \Table\ref{tab:vision_uncertainties} -- \textit{edge} features for the rotorcap, and \textit{surface points} for the magnet), and dark red indicates the best case (see \Table\ref{tab:vision_uncertainties} -- \textit{surface points} for the rotorcap, and \textit{edge} features for the magnet). Green dots represent the successful grasping area due to the gripper aligning capability.}
    \label{fig:basin_of_convergence}
  \end{figure*}
% --- /fig: basin_of_convergence ---

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{EXPERIMENTS}\label{sec:experiments}
In order to validate our approach of gripper finger design, we performed a set of experiments to assess the optimization results quantitatively, and to verify that dynamic simulation matches the observations obtained in the real-life setting. \Sections\ref{sec:results_magnet} and \ref{sec:results_rotorcap} detail the experimental settings for the two objects considered: the \textit{magnet} and the \textit{rotorcap}. After obtaining the feedback from extensive testing, the computed finger designs were employed in a real grasping scenario on an industrial platform (see \Section\ref{sec:industrial_scenario}.

For both objects, we first performed a set of simulations to show their respective \textit{basins of grasping success} (that is, the set of grasp offsets that still result in successful grasping -- see \Figure\ref{fig:basin_of_convergence}). Since exploring these basins would be prohibitively time-consuming, we opted to only test the limits of gripper capabilities along the set of selected cardinal directions in the real-world setting. We argue that such results still provide a good verification of the match between simulation and in-viva experiments.

We performed the grasping experiments using the Kuka LWR-4 robot equipped with a Mitsubishi RH-707 gripper, which we controlled using the Fast Research Interface (FRI) from Matlab. The robot was operating in its stiffest mode. To align the coordinate systems of the object fixtures and the robot, we defined a new, external coordinate system, which is determined with measurements of three positions using the tip of the robot. Experiments on both objects (the magnet and the rotor cap) followed the same procedure. For each of the objects, a \textit{nominal} grasp was defined, which was then perturbed in a structured way. The grasping was tested by placing the gripper in the perturbed pose, closing the fingers, and lifting the object. We have defined three outcomes of that procedure: \textit{success}, \textit{misalignment} (the object is grasped, but in an incorrect pose), and \textit{failure} (the object not grasped, or a collision). Each of the perturbed grasps was repeated 5 times in the real setting.

\subsection{The Magnet}
The \textit{magnet} is a small (dimensions: $72 \times 8.2 \times 2.9$ mm, weight: 12 g) metal bar, which has to be inserted into a groove in an electrical motor assembly. Such a procedure imposes tight requirements on grasping and handling precision for this object. Most importantly, the magnet has to be grasped with the distal part of the gripper, so that its designated cutout need to be be located close to the front surface of the fingers. Moreover, the magnet sits in an enclosing fixture which further limits the uncertainty at which the gripper can approach it for the grasping. The real and the simulated fixtures are presented in \Figure\ref{fig:magnet_setup}.
\label{sec:results_magnet}
% --- fig: magnet_setup ---
  \begin{figure}[!b]
    \centering
    \includegraphics[width=0.9\linewidth]{figures/magnet_setup}
    \caption{The magnet object grasping setups: the real setup (left) and the simulated setup (right). The gripper placement indicates the \textit{nominal} grasp defiend for the magnet.}
    \label{fig:magnet_setup}
  \end{figure}
% --- /fig: magnet_setup ---

Because of the magnet symmetry, and the fact that an offset in the X direction is less relevant for the handling of the object, we have decided to test the magnet grasping with perturbation in 5 axes (see \Table\ref{tab:perturbations}).

% --- tab: perturbations ---
\begin{table}
  \caption{Perturbations of the nominal grasp for the magnet and the rotorcap scenarios.}
  \label{tab:perturbations}
  \centering
  \begin{tabular}{|c|c|c|c|c|}
    \hline \multicolumn{5}{|c|}{Magnet} \\ \hline
    Y [mm] & Z [mm] & RX [$^\circ$] & RY [$^\circ$] & RZ [$^\circ$]\\ \hline
    $0\div6$ & $-6\div6$ & $0\div20$ & $0\div20$ & $0\div20$ \\ \hline \hline
    \multicolumn{5}{|c|}{Rotorcap} \\ \hline
    X [mm] & Y [mm] & RX [$^\circ$] & RY [$^\circ$] &  \\ \hline
    $0\div25$ & $-15\div15$ & $0\div45$ & $0\div45$ & \\ \hline
  \end{tabular}
\end{table}
% --- /tab: perturbations ---

\Figure\ref{fig:results_magnet_compared} shows a comparison between the results obtained in the real-world experiment (top) and the simulation (bottom). The plots show the outcome probability of the grasp at a given offset in 5 selected axes (where the possible outcomes are success, misalignment, or failure). Smoothed and re-sampled (using linear interpolation) results are shown, since the data points in the real-experiment were not picked uniformly. The grasps were sampled sparsely in the regions of obvious success or failure, and more densely in the are of transition between the two behaviours.

Overall, the simulated and real-world versions of the magnet grasping experiment show decent matches in the Y, Z, and RY axes. The grasping was more successful in the real setting for bigger values of the RZ offset. This can be explained by the compliance in the magnet fixture and the robot, which is not yet modelled in simulation. The wider area of successful grasps in simulated RX offsets is probably due to insufficient modelling of directional friction, encountered in the printed material of the finger.

% --- fig: results_magnet_compared ---
  \begin{figure*}[tbh]
    \centering
    \includegraphics[width=0.85\linewidth,height=6cm]{figures/magnet_comparison_resampled} 
    \caption{A comparison between results for the real magnet grasping setup (top) and the simulated experiment (bottom).}
    \label{fig:results_magnet_compared}
  \end{figure*}
% --- /fig: results_magnet_compared ---

\subsection{The Rotorcap}
\label{sec:results_rotorcap}
The \textit{rotorcap} is an aluminum cylindrical object (dimensions: $\oslash 32 \times 110$ mm, weight: 60 g) which is a part of electrical motor assembly that houses the rotor shaft and a set of magnets. The rotorcaps are fed on a fixture, on which the objects sit, and have to be subsequently grasped and placed in a press. The fixture, and the experimental setting are shown in \Figure\ref{fig:rotorcap_setup}.
% --- fig: rotorcap_setup ---
  \begin{figure}[!b]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/rotorcap_setup}
    \caption{The rotorcap object grasping setups: the real setup (left) and the simulated setup (right). Fixture on which the rotorcap sits is visible on the left. The gripper placement indicates the \textit{nominal} grasp defined for the rotorcap.}
    \label{fig:rotorcap_setup}
  \end{figure}
% --- /fig: rotorcap_setup ---

Since the rotorcap is cylindrically symmetric, and the offset in grasping along the Z axis is not important in the object handling, we have tested grasping perturbation in 4 directions (see \Table\ref{tab:perturbations}).\Figure\ref{fig:results_rotorcap_compared} presents a comparison of the results for the real-world and the simulated scenario. Same as in the magnet experiment case, the plots are smoothed and interpolated between the non-uniform data-points used.

The simulated and the real-world results show a good match, with the simulated version indicating slightly smaller basins of success. The feedback from simulation can be then deemed more restrictive, and should result in more optimal designs with a wide margin of error. The discrepancy between the results is due to the lack of the compliance modelling. The simulated device is rigid, and does not allow the robot to adapt to the forces imposed by the object or the fixture.

% --- fig: results_rotorcap_real ---
%  \begin{figure*}[!tb]
%    \centering
%    \begin{tabular}{cc}
%      \includegraphics[width=0.45\linewidth]{figures/rotorcap_real_samples} &
%      \includegraphics[width=0.45\linewidth]{figures/rotorcap_real_lines}
%    \end{tabular}
%    \caption{The rotorcap grasping results for the real setup: samples (on the left) and corresponding outcome plot (on the right). \commentAW{temporary plot!}}
%    \label{fig:results_rotorcap_real}
%  \end{figure*}
% --- /fig: results_rotorcap_real ---

% --- fig: results_rotorcap_compared ---
  \begin{figure*}[tbh]
    \centering
    \includegraphics[width=0.85\linewidth,height=6cm]{figures/rotorcap_comparison_resampled} 
    \caption{A comparison between results for the real rotorcap grasping setup (top) and the simulated experiment (bottom).}
    \label{fig:results_rotorcap_compared}
  \end{figure*}
% --- /fig: results_rotorcap_compared ---

% --- fig: qualitative_alignment ---
  \begin{figure}[!t]
    \centering
    \begin{tabular}{cc}
      \includegraphics[width=0.45\linewidth,height=3cm]{figures/magnet_success} &
      \includegraphics[width=0.45\linewidth,height=3cm]{figures/magnet_misaligned} \\
      \includegraphics[width=0.45\linewidth,height=3cm]{figures/rotorcap_success} &
      \includegraphics[width=0.45\linewidth,height=3cm]{figures/rotorcap_misaligned}     
    \end{tabular}
    \caption{Qualitative alignment for the tested objects. Top row -- from left to right: successful magnet grasp, misaligned magnet grasp. Bottom row -- from left to right: succesful rotorcap grasp, misaligned rotorshaft grasp.}
    \label{fig:qualitative_alignment}
  \end{figure}
% --- /fig: qualitative_alignment ---

\subsection{Application in the Industrial Scenario}\label{sec:industrial_scenario}
After testing the computed fingers in simulation and on a test platform, we were able to confirm that the gripper performs as expected, and is able to compensate for the vision uncertainties involved. The designed gripper fingers were applied in the industrial use-case, in which an electrical motor was assembled.  \Figure\ref{fig:acat_scenario} presents the grasps executed in the industrial setting. The robotic arm used was the Universal Robot model UR-5, which was equipped with a WSG50 gripper. The vision system was a combined Carmine and stereo system, which has also been used for the uncertainty computations reported in section \ref{sec:pose_estimation}.

\begin{figure*}[!tbh]
    \centering
    \begin{tabular}{cc}
      1)\includegraphics[width=0.33\linewidth,height=4cm]{figures/vision_setup1} &
      2)\includegraphics[width=0.33\linewidth,height=4cm]{figures/aau_rotorcap_1} \\
      3)\includegraphics[width=0.33\linewidth,height=4cm]{figures/aau_magnet_2} &
      4)\includegraphics[width=0.33\linewidth,height=4cm]{figures/aau_magnet_4}    
    \end{tabular}
    \caption{The application of the computed gripper design in the industrial context. In order: 1) The vision system used, 2) Grasping the rotorcap, 3) Grasping the magnet, 4) Inserting the magnet into the rotorcap assembly.}
    \label{fig:acat_scenario}
  \end{figure*}

%\FloatBarrier


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{CONCLUSION}
In this paper we have presented a comprehensive approach to the problem of the optimal gripper design to compensate for the vision system induced pose estimation uncertainties. We have analyzed the errors in vision based pose estimation, and defined these as the task context for our automated simulation-based finger designing procedure. We have computed the optimal geometry of fingers for two objects in a real industrial-based scenario. These geometries have then been merged into a single finger design, that was then used in the experiments executed both in simulation, on a real test platform, and subsequently in a real industrial scenario.

We were able to obtain a decent match between the results from simulation and from the real-world experiments. The few cases in which the outcomes were mis-matched suggest that the feedback obtained from simulation is often more restrictive, and thus results in more robust designs. We argue that by this we can affirm the validity of our proposed methodology.

Still, we are aware that there are sufficient discrepancies between the simulated and real results to justify further development of our simulation environment, and more extensive testing. In our future work we plan to enhance our suite with compliance, and include more elaborate friction models. Furthermore, in our future experiments we plan to test a new physics engine designed for industrial assembly simulation \cite{tntphysics}.

\addtolength{\textheight}{-12cm}   % This command serves to balance the column lengths
                                  % on the last page of the document manually. It shortens
                                  % the textheight of the last page by a suitable amount.
                                  % This command does not take effect until the next page
                                  % so it should come on the page before the last. Make
                                  % sure that you do not shorten the textheight too much.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{ACKNOWLEDGMENT}
The research leading to these results has received funding from the European Communitys Seventh Framework Programme FP7/2007-2013 (Programme and Theme: ICT-2011.2.1, Cognitive Systems and Robotics) under grant agreement no. 600578, ACAT and by the Danish Agency for Science, Technology and Innovation, project CARMEN. 
\FloatBarrier

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bibliographystyle{IEEEtran}
\bibliography{references}




\end{document}
