function [ T ] = rawToTransforms( raw )
% Converts raw pose data into transforms

T = {};

n = length(raw);
for i = [1 : n]
    pos = raw(i, 1:3);
    rot = raw(i, 4:12);
    
    t = [
        rot(1:3) pos(1);
        rot(4:6) pos(2);
        rot(7:9) pos(3);
        0 0 0 1
    ];

    T = [T; t];
end

end

