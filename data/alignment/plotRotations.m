function plotRotations( T )
% plots unit versors of rotations

vx = [1; 0; 0; 0];
vy = [0; 1; 0; 0];
vz = [0; 0; 1; 0];

xs = []; ys = []; zs = [];

for i = [1 : length(T)]
    x = T{i} * vx;
    x = x(1:3)';
    xs = [xs; x];
    
    y = T{i} * vy;
    y = y(1:3)';
    ys = [ys; y];
    
    z = T{i} * vz;
    z = z(1:3)';
    zs = [zs; z];
end

scatter3(xs(:, 1), xs(:, 2), xs(:, 3), 'red'); hold on;
scatter3(ys(:, 1), ys(:, 2), ys(:, 3), 'green');
scatter3(zs(:, 1), zs(:, 2), zs(:, 3), 'blue'); hold off;

end

