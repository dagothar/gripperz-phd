function plot_blob(data, i1, i2)

    s = data(data(:, 7) == 2, :);
    m = data(data(:, 7) == 1, :);
    f = data(data(:, 7) == 0, :);
    
    figure;
    scatter(s(:, i1), s(:, i2), 'green', 'filled'); hold on;
    scatter(m(:, i1), m(:, i2), 'yellow', 'filled');
    scatter(f(:, i1), f(:, i2), 'red', 'filled'); hold off;

end

