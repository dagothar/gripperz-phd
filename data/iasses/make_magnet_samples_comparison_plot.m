function make_magnet_samples_comparison_plot( real_data, sim_data )

    y_real = extract_axis_data(real_data, 2);
    z_real = extract_axis_data(real_data, 3);
    rx_real = extract_axis_data(real_data, 4);
    ry_real = extract_axis_data(real_data, 5);
    rz_real = extract_axis_data(real_data, 6);
    
    y_sim = extract_axis_data(sim_data, 2);
    z_sim = extract_axis_data(sim_data, 3);
    rx_sim = extract_axis_data(sim_data, 4);
    ry_sim = extract_axis_data(sim_data, 5);
    rz_sim = extract_axis_data(sim_data, 6);

    figure;
    subplot(2, 5, 1);
    plot_stacked_samples(y_real, 3);
    xlim([0 0.006]); ylim([0 2]); set(gca, 'Xtick', [0:0.001:0.006]);
    title('Y real'); xlabel('offset'); ylabel('S/M/F');
    
    subplot(2, 5, 2);
    plot_stacked_samples(z_real, 3);
    xlim([-0.006 0.006]); ylim([0 2]); set(gca, 'Xtick', [-0.006:0.002:0.006]);
    title('Z real'); xlabel('offset'); ylabel('S/M/F');
    
    subplot(2, 5, 3);
    plot_stacked_samples(rx_real, 3);
    xlim([0 20]); ylim([0 2]); set(gca, 'Xtick', [0:2:20]);
    title('RX real'); xlabel('offset'); ylabel('S/M/F');
    
    subplot(2, 5, 4);
    plot_stacked_samples(ry_real, 3);
    xlim([0 20]); ylim([0 2]); set(gca, 'Xtick', [0:2:20]);
    title('RY real'); xlabel('offset'); ylabel('S/M/F');
    
    subplot(2, 5, 5);
    plot_stacked_samples(rz_real, 3);
    xlim([0 20]); ylim([0 2]); set(gca, 'Xtick', [0:2:20]);
    title('RZ real'); xlabel('offset'); ylabel('S/M/F');
    
    subplot(2, 5, 6);
    plot_stacked_samples(y_sim, 3);
    xlim([0 0.006]); ylim([0 2]); set(gca, 'Xtick', [0:0.001:0.006]);
    title('Y sim'); xlabel('offset'); ylabel('S/M/F');
    
    subplot(2, 5, 7);
    plot_stacked_samples(z_sim, 3);
    xlim([-0.006 0.006]); ylim([0 2]); set(gca, 'Xtick', [-0.006:0.002:0.006]);
    title('Z sim'); xlabel('offset'); ylabel('S/M/F');
    
    subplot(2, 5, 8);
    plot_stacked_samples(rx_sim, 3);
    xlim([0 20]); ylim([0 2]); set(gca, 'Xtick', [0:2:20]);
    title('RX sim'); xlabel('offset'); ylabel('S/M/F');
    
    subplot(2, 5, 9);
    plot_stacked_samples(ry_sim, 3);
    xlim([0 20]); ylim([0 2]); set(gca, 'Xtick', [0:2:20]);
    title('RY sim'); xlabel('offset'); ylabel('S/M/F');
    
    subplot(2, 5, 10);
    plot_stacked_samples(rz_sim, 3);
    xlim([0 20]); ylim([0 2]); set(gca, 'Xtick', [0:2:20]);
    title('RZ sim'); xlabel('offset'); ylabel('S/M/F');

end

