function make_magnet_resampled_lines_plot( real_data, sim_data, w )

    h_pos = 0.00025;
    h_ang = 1;
    w_pos = 0.001;
    w_ang = 2;
    
    fs = 20

    if (nargin < 3) 
        w = 3;
    end

    %% load data
    y_real = extract_axis_data(real_data, 2);
    z_real = extract_axis_data(real_data, 3);
    rx_real = extract_axis_data(real_data, 4);
    ry_real = extract_axis_data(real_data, 5);
    rz_real = extract_axis_data(real_data, 6);
    
    y_sim = extract_axis_data(sim_data, 2);
    z_sim = extract_axis_data(sim_data, 3);
    rx_sim = extract_axis_data(sim_data, 4);
    ry_sim = extract_axis_data(sim_data, 5);
    rz_sim = extract_axis_data(sim_data, 6);
    
    %% resample
    y_real = resample_linear(y_real, [0:h_pos:0.006], w_pos);
    z_real = resample_linear(z_real, [-0.006:h_pos*2:0.006], w_pos);
    rx_real = resample_linear(rx_real, [0:h_ang:20], w_ang);
    ry_real = resample_linear(ry_real, [0:h_ang:20], w_ang);
    rz_real = resample_linear(rz_real, [0:h_ang:20], w_ang);
    
    y_sim = resample_linear(y_sim, [0:h_pos:0.006], w_pos);
    z_sim = resample_linear(z_sim, [-0.006:h_pos*2:0.006], w_pos);
    rx_sim = resample_linear(rx_sim, [0:h_ang:20], w_ang);
    ry_sim = resample_linear(ry_sim, [0:h_ang:20], w_ang);
    rz_sim = resample_linear(rz_sim, [0:h_ang:20], w_ang);

    %% make plots
    figure;
    subplot(2, 5, 1);
    plot_lines(y_real, w);
    xlim([0 0.006]); ylim([0 1.1]); set(gca, 'Xtick', [0:0.001:0.006]);
    title('Y real', 'FontSize', fs); xlabel('offset', 'FontSize', fs); ylabel('S/M/F', 'FontSize', fs);
    set(gca, 'FontSize', fs);
    
    subplot(2, 5, 2);
    plot_lines(z_real, w);
    xlim([-0.006 0.006]); ylim([0 1.1]); set(gca, 'Xtick', [-0.006:0.002:0.006]);
    title('Z real', 'FontSize', fs); xlabel('offset', 'FontSize', fs); ylabel('S/M/F', 'FontSize', fs);
    set(gca, 'FontSize', fs);
    
    subplot(2, 5, 3);
    plot_lines(rx_real, w);
    xlim([0 20]); ylim([0 1.1]); set(gca, 'Xtick', [0:5:20]);
    title('RX real', 'FontSize', fs); xlabel('offset', 'FontSize', fs); ylabel('S/M/F', 'FontSize', fs);
    set(gca, 'FontSize', fs);
    
    subplot(2, 5, 4);
    plot_lines(ry_real, w);
    xlim([0 20]); ylim([0 1.1]); set(gca, 'Xtick', [0:5:20]);
    title('RY real', 'FontSize', fs); xlabel('offset', 'FontSize', fs); ylabel('S/M/F', 'FontSize', fs);
    set(gca, 'FontSize', fs);
    
    subplot(2, 5, 5);
    plot_lines(rz_real, w);
    xlim([0 20]); ylim([0 1.1]); set(gca, 'Xtick', [0:5:20]);
    title('RZ real', 'FontSize', fs); xlabel('offset', 'FontSize', fs); ylabel('S/M/F', 'FontSize', fs);
    set(gca, 'FontSize', fs);
    
    subplot(2, 5, 6);
    plot_lines(y_sim, w);
    xlim([0 0.006]); ylim([0 1.1]); set(gca, 'Xtick', [0:0.001:0.006]);
    title('Y sim', 'FontSize', fs); xlabel('offset', 'FontSize', fs); ylabel('S/M/F', 'FontSize', fs);
    set(gca, 'FontSize', fs);
    
    subplot(2, 5, 7);
    plot_lines(z_sim, w);
    xlim([-0.006 0.006]); ylim([0 1.1]); set(gca, 'Xtick', [-0.006:0.002:0.006]);
    title('Z sim', 'FontSize', fs); xlabel('offset', 'FontSize', fs); ylabel('S/M/F', 'FontSize', fs);
    set(gca, 'FontSize', fs);
    
    subplot(2, 5, 8);
    plot_lines(rx_sim, w);
    xlim([0 20]); ylim([0 1.1]); set(gca, 'Xtick', [0:5:20]);
    title('RX sim', 'FontSize', fs); xlabel('offset', 'FontSize', fs); ylabel('S/M/F', 'FontSize', fs);
    set(gca, 'FontSize', fs);
    
    subplot(2, 5, 9);
    plot_lines(ry_sim, w);
    xlim([0 20]); ylim([0 1.1]); set(gca, 'Xtick', [0:5:20]);
    title('RY sim', 'FontSize', fs); xlabel('offset', 'FontSize', fs); ylabel('S/M/F', 'FontSize', fs);
    set(gca, 'FontSize', fs);
    
    subplot(2, 5, 10);
    plot_lines(rz_sim, w);
    xlim([0 20]); ylim([0 1.1]); set(gca, 'Xtick', [0:5:20]);
    title('RZ sim', 'FontSize', fs); xlabel('offset', 'FontSize', fs); ylabel('S/M/F', 'FontSize', fs);
    set(gca, 'FontSize', fs);

end

