function make_rotorcap_resampled_comparison_plot( real_data, sim_data, w )

    h_pos = 0.001;
    h_ang = 2;
    w_pos = 0.0025;
    w_ang = 5;
    
    fs = 20

    if (nargin < 3) 
        w = 3;
    end

    %% load data
    x_real = extract_axis_data(real_data, 1);
    y_real = extract_axis_data(real_data, 2);
    rx_real = extract_axis_data(real_data, 4);
    ry_real = extract_axis_data(real_data, 5);
    
    x_sim = extract_axis_data(sim_data, 1);
    y_sim = extract_axis_data(sim_data, 2);
    rx_sim = extract_axis_data(sim_data, 5);
    ry_sim = extract_axis_data(sim_data, 6);
    
    %% resample
    x_real = resample_linear(x_real, [0:h_pos:0.03], w_pos);
    y_real = resample_linear(y_real, [-0.015:h_pos:0.015], w_pos);
    rx_real = resample_linear(rx_real, [0:h_ang:45], w_ang);
    ry_real = resample_linear(ry_real, [0:h_ang:45], w_ang);
    
    x_sim = resample_linear(x_sim, [0:h_pos:0.03], w_pos);
    y_sim = resample_linear(y_sim, [-0.015:h_pos:0.015], w_pos);
    rx_sim = resample_linear(rx_sim, [0:h_ang:45], w_ang);
    ry_sim = resample_linear(ry_sim, [0:h_ang:45], w_ang);
    
    %% make figure
    figure;
    subplot(2, 4, 1);
    plot_lines(x_real, 3);
    ylim([0 1.1]); set(gca, 'Xtick', [0:0.01:0.03]);
    title('X real', 'FontSize', fs); xlabel('offset', 'FontSize', fs); ylabel('S/M/F', 'FontSize', fs); set(gca, 'FontSize', fs);
    
    subplot(2, 4, 2);
    plot_lines(y_real, 3);
    xlim([-0.015 0.015]); ylim([0 1.1]); set(gca, 'Xtick', [-0.01:0.01:0.01]);
    title('Y real', 'FontSize', fs); xlabel('offset', 'FontSize', fs); ylabel('S/M/F', 'FontSize', fs); set(gca, 'FontSize', fs);
    
    subplot(2, 4, 3);
    plot_lines(rx_real, 3);
    xlim([0 45]); ylim([0 1.1]); set(gca, 'Xtick', [0:10:45]);
    title('RX real', 'FontSize', fs); xlabel('offset', 'FontSize', fs); ylabel('S/M/F', 'FontSize', fs); set(gca, 'FontSize', fs);
    
    subplot(2, 4, 4);
    plot_lines(ry_real, 3);
    xlim([0 45]); ylim([0 1.1]); set(gca, 'Xtick', [0:10:45]);
    title('RY real', 'FontSize', fs); xlabel('offset', 'FontSize', fs); ylabel('S/M/F', 'FontSize', fs); set(gca, 'FontSize', fs);
    
    subplot(2, 4, 5);
    plot_lines(x_sim, 3);
    ylim([0 1.1]); set(gca, 'Xtick', [0:0.01:0.03]);
    title('X sim', 'FontSize', fs); xlabel('offset', 'FontSize', fs); ylabel('S/M/F', 'FontSize', fs); set(gca, 'FontSize', fs);
    
    subplot(2, 4, 6);
    plot_lines(y_sim, 3);
    xlim([-0.015 0.015]); ylim([0 1.1]); set(gca, 'Xtick', [-0.01:0.01:0.01]);
    title('Y sim', 'FontSize', fs); xlabel('offset', 'FontSize', fs); ylabel('S/M/F', 'FontSize', fs); set(gca, 'FontSize', fs);
    
    subplot(2, 4, 7);
    plot_lines(rx_sim, 3);
    xlim([0 45]); ylim([0 1.1]); set(gca, 'Xtick', [0:10:45]);
    title('RX sim', 'FontSize', fs); xlabel('offset', 'FontSize', fs); ylabel('S/M/F', 'FontSize', fs); set(gca, 'FontSize', fs);
    
    subplot(2, 4, 8);
    plot_lines(ry_sim, 3);
    xlim([0 45]); ylim([0 1.1]); set(gca, 'Xtick', [0:10:45]);
    title('RY sim', 'FontSize', fs); xlabel('offset', 'FontSize', fs); ylabel('S/M/F', 'FontSize', fs); set(gca, 'FontSize', fs);

end

